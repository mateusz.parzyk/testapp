# TestApp

To run this project:
1. clone and run fake api from `https://github.com/testdrivenio/fake-token-api`
2. go to cloned fake api and Install Dependencies - `npm install`
3. start fake api with `npm start`
4. clone this repo
5. go to the repo and run `npm install`
6. start this project with `ng serve`