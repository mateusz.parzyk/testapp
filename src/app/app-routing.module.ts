import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogInComponent } from "./log-in/log-in.component";
import { HomePageComponent } from "./home-page/home-page.component";
import { DashboardComponent } from "./dashboard/dashboard.component";

import { AuthGuardService as AuthGuard } from "./services/auth-guard.service";


const routes: Routes = [
  {
    path: '',
    component: HomePageComponent,
  },
  
  {
    path: 'log-in',
    component: LogInComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [ AuthGuard ]
  },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
