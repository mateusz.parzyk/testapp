import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from "rxjs";

import { LogOut } from '../store/actions/auth.actions';
import { AppState, selectAuthState } from '../store/app.states';
import { GetStatus } from '../store/actions/auth.actions';
import { AuthService } from "../services/auth.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  getState: Observable<any>;
  isAuthenticated: false;
  username = null;
  user = null;
  errorMessage = null;

  constructor(
    private store: Store<AppState>,
    private authService: AuthService,
  ) {
    this.getState = this.store.select(selectAuthState);
  }

  ngOnInit() {
    this.store.dispatch(new GetStatus);
    this.username = this.authService.getUser()
    this.getState.subscribe((state) => {
      this.isAuthenticated = state.isAuthenticated;
      this.user = state.user;
      this.errorMessage = state.errorMessage;
    });
  }

  logOut(): void {
    this.store.dispatch(new LogOut);
  }

}
